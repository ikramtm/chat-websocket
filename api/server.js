var express = require('express');
var http = require('http')
var socketio = require('socket.io');
var app = express();
const bodyParser = require('body-parser');
const server = http.Server(app);

const PORT = process.env.PORT || 3005;

// app.listen(PORT, () => {
//     console.log('Server Started on http://localhost:%s',PORT);
//     console.log('Press CTRL + C to stop server');
// });

// const socketio = require('socket.io');
var socketio = require('socket.io');
var websocket = socketio(server);

app.use(bodyParser.json());

websocket.on('connection', (socket) => {
    console.log('Client is connected');
    socket.on('message', function(data){
        console.log(data.data);
    })
});

server.listen(3005, () => console.log('listening on *:3005'));

