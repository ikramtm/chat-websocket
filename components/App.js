import React, { Component } from 'react';
import SocketIOClient from 'socket.io-client';
import axios from 'axios';
import {
  AppRegistry,
  Alert,
  StyleSheet,
  Button,
  ListView,
  TextInput,
  Text,
  View
} from 'react-native';


const onButtonPress = () => {
  Alert.alert('Button has been pressed!');
};
let userId = 2123;

export default class chat extends Component {
    constructor(props){
        super(props);
        this.state = {
            text : '',
            input:['Hi', 'How are you?'],
        }
        this.emitMassage =  this.emitMassage.bind(this);
        this.socket = SocketIOClient('http://localhost:3005');
        this.socket.on('message', this.emitMassage);
        this.sendMessage = this.sendMessage.bind(this);
    }  
    sendMessage(){
        let newInput = this.state.text;
        let newChat = [];
        if (newInput.length > 0){
          newChat = this.state.input.concat(newInput)
          this.setState({
            input:newChat,
            text:''
          })
        }else{
          Alert.alert('Please put in text');
        }
    }
    componentDidUpdate(prevProps,prevState){
      if(prevState.input !== this.state.input){
        this.emitMassage();
      }
    }
    emitMassage(){
      this.socket.emit('message',{data : this.state.input[this.state.input.length -1]}, ()=>{
        alert('data');
      })
    }
  render() {
    this.dataSource = new ListView.DataSource({
            rowHasChanged: (row1,row2) => row1 !== row2
        });
        var dataSource = this.dataSource.cloneWithRows(this.state.input);
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to Ballprk Chat!
        </Text>
        <View style={styles.chat}>
            <ListView
              dataSource={dataSource}
              renderRow={(rowData) => <Text>{rowData}</Text>}
            />
        </View>
        <TextInput
        style={{paddingLeft:3,borderRadius:5,margin:20,height: 40, borderColor: 'gray', borderWidth: 1}}
        onChangeText={(text) => this.setState({text})} 
        placeholder="Message"
        value={this.state.text}
        />
        <View>
            <Button
            onPress={onButtonPress}
            title="Start Connection"
            color="#841584"
            accessibilityLabel="Learn more about purple"
            />
            <Button
            onPress={this.sendMessage}
            title="Send"
            color="#841584"
            accessibilityLabel="Ok, Great!"
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  chat: {
    height:400,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});